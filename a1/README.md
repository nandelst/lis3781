> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Douglas Nandelstadt

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code (optional)
5. Bitbucket repo links:

	a) this assignment, and
	
	b) the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex3. SQL Solution
* git commands w/ short descriptions

#### Git commands w/short descriptions:

1. git init - Initializes a new Git repository.
2. git status - Lets you see the status of your repository such as new changes or tracked files.
3. git add - Stages a specific file or your directory for updates.
4. git commit - Saves your updates to the local repository.
5. git push - Uploads your local repository to a remote one.
6. git pull - Downloads a remote repository and updates your local repository.
7. git tag - Adds a reference number to a specific file to mark a version of it.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.PNG)

*Screenshot of A1 ERD*:

![Assignment 1 ERD Screenshot](img/erd.PNG)

*Screenshot of A1 Ex3 Solution*:

![Assignment 1 Example 3 Screenshot](img/e3.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/nandelst/bitbucketstationlocations/ "Bitbucket Station Locations")
