> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS3781 - Advanced Database Management

## Douglas Nandelstadt

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install AMPPS
	- Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutorial (bitbucketstationlocations)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create database using SQL
	- SQL server commands

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create and populate Oracle tables
	- Practice Oracle commands

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Create database using T-SQL
	- Practice procedures and triggers with T-SQL

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Expand database using T-SQL

6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Create database using SQL
	- Practice procedures and triggers with SQL

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- MongoDB and Atlas cluster setup
	- Practice NoSQL commands