# LIS3781 - Advanced Database Management

## Douglas Nandelstadt

### Project 1 Requirements:

*Two Parts:*

1. Entity Relationship Diagram
2. Chapter Questions (Ch 13)

#### README.md file should include the following item:

* Screenshot of Project 1 ERD

#### Assignment Screenshots:

*Screenshot of Project 1 ERD*:

![Project 1 ERD Screenshot](img/p1.PNG)