# LIS3781 - Advanced Database Management

## Douglas Nandelstadt

### Assignment 3 Requirements:

*Two Parts:*

1. Develop Customer, Commodity, and Order tables in Oracle
2. Chapter Questions (Ch 12)

#### README.md file should include the following items:

* Screenshots of SQL code
* Screenshots of populated tables

#### Assignment Screenshots:

*Screenshots of SQL code*:

| Page 1 | Page 2 | Page 3 |
| :---: | :---: | :---: |
| ![Page 1 of SQL code](img/code1.PNG) | ![Page 2 of SQL code](img/code2.PNG) | ![Page 3 of SQL code](img/code3.PNG) |

*Screenshots of populated tables*:

| Customer table | Commodity table | Order table |
| :---: | :---: | :---: |
| ![Screenshot of customer table](img/customer.PNG) | ![Screenshot of commodity table](img/commodity.PNG) | ![Screenshot of order table](img/order.PNG) |