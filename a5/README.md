# LIS3781 - Advanced Database Management

## Douglas Nandelstadt

### Assignment 5 Requirements:

*Two Parts:*

1. Entity Relationship Diagram designed with T-SQL
2. Chapter Questions (Ch. 15)

#### README.md file should include the following item:

* Screenshot of Assignment 5 ERD
* Link to Assignment 5 SQL code

#### Assignment Screenshots:

*Screenshot of Assignment 5 ERD*:

![Assignment 5 ERD Screenshot](img/ERD.PNG)

#### Assignment Links:

*Link to Assignment 5 SQL code*:
[a5.sql](docs/a5.sql "Link to assignment 5 SQL")