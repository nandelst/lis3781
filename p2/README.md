# LIS3781 - Advanced Database Management

## Douglas Nandelstadt

### Project 2 Requirements:

*Three parts*:

1. MongoDB installation
2. MongoDB Questions
3. Chapter Questions (Ch. 16)

	
#### README.md file should include the following item(s):

* Screenshot of at least one MongoDB shell command(s), (e.g., show collections)

#### Assignment Screenshots:

*Screenshot of MongoDB shell commands*:

![MongoDB shell commands](img/shell.PNG)

