# LIS3781 - Advanced Database Management

## Douglas Nandelstadt

### Assignment 4 Requirements:

*Two Parts:*

1. Entity Relationship Diagram designed with T-SQL
2. Chapter Questions (Ch. 14)

#### README.md file should include the following item:

* Screenshot of Assignment 4 ERD
* Link to Assignment 4 SQL code

#### Assignment Screenshots:

*Screenshot of Assignment 4 ERD*:

![Assignment 4 ERD Screenshot](img/erd.PNG)

#### Assignment Links:

*Link to Assignment 4 SQL code*:
[a4.sql](docs/a4.sql "Link to assignment 4 SQL")