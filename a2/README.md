# LIS3781 - Advanced Database Management

## Douglas Nandelstadt

### Assignment 2 Requirements:

*Two Parts:*

1. Develop Company & Customer Tables Using SQL
2. Chapter Questions (Ch 11)

#### README.md file should include the following items:

* Screenshots of SQL code
* Screenshots of populated tables

#### Assignment Screenshots:

*Screenshots of SQL code*:

| Page 1 | Page 2 | Page 3 |
| :---: | :---: | :---: |
| ![Page 1 of SQL code](img/one.PNG) | ![Page 2 of SQL code](img/two.PNG) | ![Page 3 of SQL code](img/three.PNG) |

*Screenshots of populated tables*:

| Company table | Customer table |
| :---: | :---: |
| ![Screenshot of company table](img/company.PNG) | ![Screenshot of customer table](img/customer.PNG) |